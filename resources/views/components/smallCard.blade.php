<div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-{{ $cor }} shadow h-100 p-0 col-12">
        <div class="card-header py-3 ">
            <div class="container row justify-content-left h-100 w-100 px-0">
                <div class=" px-2 text-{{ $cor }}">
                    <i class="fas fa-fw fa-{{ $iconHeader }} "></i>
                </div>
                <h6 class="m-0 font-weight-bold text-{{ $cor }}">{{ $titulo }}</h6>
            </div>
        </div>
        <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="row justify-content-center">
                    <div class="row justify-content-center">
                        @if(isset($icon))
                            <!--i class="{{$icon.' mb-3 '}}"></i-->
                            <i class="{{$icon.' mb-3 '}} text-{{ $cor }}"></i>
                        @endif
                    </div>                    
                </div>   
                @if(isset($valor))
                <div class="row justify-content-center" >
                <div class="h1 mb-0 font-weight-bold text-grey-800 ">{{ $valor }}</div>
                    <input id="pontos-ganhos" value="{{ session('pontos-ganhos') }}" type="hidden">
                    @if(isset($id) && $id != null )
                        <input id="{{ $id }}" value="{{ session('pontuacaoExigida') }}" type="hidden">
                    @else
                        <input value="{{ session('pontuacaoExigida') }}" type="hidden">
                    @endif
                </div>
                @endif              
                <div class="row no-gutters align-items-center justify-content-center">
                </div>
            </div>
        </div>
            <hr>
            <div class="row justify-content-center">
                <a class="btn btn-{{$cor}} text-white" href="{{ $cardLink ?? '#' }}">
                    Mais detalhes
                </a>
            </div>
        </div>
    </div>
</div>