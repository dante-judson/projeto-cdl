@extends('template')

@section('titulo','Lista de Cursos')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Lista de Cursos</h1>
@endsection

@section('conteudo')
    <a href="/adm/cursos/adicionar" class="d-none d-inline-block btn btn-sm btn-primary shadow-sm mb-3"><i class="fas fa-book fa-sm text-white-50"></i> Adicionar Curso </a>
    <hr class="mt-0">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <thead>
                <tr>
                    <td>Código </td>
                    <td>Nome</td>
                    <td>Editar</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($cursos as $c => $curso)                        
                    <tr>
                        <td class="w-25">{{ $curso->id}} </td>
                        <td class="w-75">{{ $curso->nome }} </td>
                        <td class="w-auto">
                            <a class="btn btn-primary w-100" href="/adm/cursos/editar/{{ $curso->id}}"><i class="fas fa-edit fa-sm text-white-50"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection