@extends('template')

@section('titulo','Lista de Turmas')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Lista de Turmas</h1>
@endsection

@section('conteudo')
    <a href="/adm/turmas/adicionar" class="d-none d-inline-block btn btn-sm btn-primary shadow-sm mb-3"><i class="fas fa-users fa-sm text-white-50"></i> Abrir Turma </a>
    <hr class="mt-0">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable">
            <thead>
                <tr>
                    <td class="w-auto">ID da Turma</td>
                    <td class="w-auto">Curso</td>
                    <td class="w-auto">Coordenador</td>
                    <td class="w-auto">Turno</td>
                    <td class="w-auto">Editar</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($listTurmas as $t => $turma)                        
                    <tr>
                        <td>{{ $turma[0]->pivot->id}} </td>
                        <td class="align-content-center justify-content-center">{{ $turma[0]->nome }} </td>
                        <td>{{ $turma[1] }} </td>
                        <td>{{ $turma[0]->pivot->turno }} </td>
                        <td><a href="/adm/turmas/editar/{{ $turma[0]->pivot->id }}" 
                            class="w-100 d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                <i class="fas fa-edit fa-sm"></i> 
                            </a> 
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection