@extends('template')

@section('titulo','Abrir Turma')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">{{$turma->id == null ? 'Adicionar' : 'Editar'}} Turma</h1>
@endsection

@section('conteudo')

    <form method="POST" action="/adm/turmas/upsert">
        {{ csrf_field() }}
        <input type="hidden" value="{{ $turma->id }}" name="id">
        <div class="form-group">
            <label for="curso_id">Curso:</label>
            <select name="curso_id" class="form-control" required>
                @if ($turma->id == null)
                    <option value="" selected >
                        Selecione um Curso
                    </option>
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}" >
                            {{ $curso->nome }}
                        </option>                    
                    @endforeach
                @else                    
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}" {{ $curso->id == $turma->curso_id ? 'selected' : '' }}>
                            {{ $curso->nome }}
                        </option>                  
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="usuario_id">Coordenador:</label>
            <select name="usuario_id" class="form-control" required>
                @if ($turma->id == null)
                    <option value="" selected >
                        Selecione um Coordenador
                    </option>
                    @foreach ($usuarios as $usuario)
                        <option value="{{ $usuario->id }}" >
                            {{ $usuario->nome }}
                        </option>                    
                    @endforeach
                @else                    
                    @foreach ($usuarios as $usuario)
                        <option value="{{ $usuario->id }}" {{ $usuario->id == $turma->usuario_id ? 'selected' : '' }}>
                            {{ $usuario->nome }}
                        </option>                  
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="turno">Turno:</label>
            <select name="turno" class="form-control" required>
                @if ($turma->id == null)
                    <option value="" selected >
                        Selecione um Turno
                    </option>
                    @foreach ($turnos as $turno)
                        <option value="{{ $turno }}" >
                            {{ $turno }}
                        </option>                    
                    @endforeach
                @else                    
                    @foreach ($turnos as $turno)
                        <option value="{{ $turno }}" {{ $turno == $turma->turno ? 'selected' : '' }}>
                            {{ $turno }}
                        </option>                  
                    @endforeach
                @endif
            </select>
        </div>       

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value='Salvar'>
        </div>
    </form>
@endsection