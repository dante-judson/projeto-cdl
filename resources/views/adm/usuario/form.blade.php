@extends('template')

@section('titulo','Adicionar usuarios')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">{{$usuario->id == null ? 'Adicionar' : 'Editar'}} Usuário</h1>
@endsection

@section('conteudo')
    <form method="POST" action="/adm/usuario/upsert">
        {{ csrf_field() }}
        <input type="hidden" value="{{ $usuario->id }}" name="id">
        <div class="form-group">
        <label for="login">Login:</label>
        <input class="form-control" type="text" value="{{ $usuario->login }}" name="login">
        </div>

        <div class="form-group">
        <label for="login">Nome:</label>
        <input class="form-control" type="text" value="{{ $usuario->nome }}" name="nome">
        </div>

        <div class="form-group">
        <label for="login">Email:</label>
        <input class="form-control" type="text" value="{{ $usuario->email }}" name="email">
        </div>

        <div class="form-group">
        <label for="login">CPF:</label>
        <input class="form-control" type="text" value="{{ $usuario->cpf }}" name="cpf">
        </div>

        <div class="form-group">
        <label for="login">Perfil:</label>
        <select name="perfil_id" class="form-control">
            @foreach ($perfils as $perfil)
                <option value="{{ $perfil->id }}" {{$perfil->id == $usuario->perfil_id?'selected':''}}>
                    {{ $perfil->valor }}
                </option>
            @endforeach
        </select>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="{{$usuario->id == null ? 'Adicionar' : 'Editar'}}">
        </div>

    </form>

@endsection