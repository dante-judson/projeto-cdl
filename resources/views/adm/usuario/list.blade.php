@extends('template')

@section('titulo','Listar usuarios')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Lista de Usuários</h1>
@endsection

@section('conteudo')
    <a href="/adm/usuario/form" class="d-none d-inline-block btn btn-sm btn-primary shadow-sm mb-3"><i class="fas fa-user-plus fa-sm text-white-50"></i> Adicionar Usuário </a>
    <hr class="mt-0">
    @if(empty($usuarios) or sizeof($usuarios) == 0)
        Nenhum usuário cadastrado.
    @else
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>CPF</th>
                        <th>Perfil</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usuarios as $usuario)
                        <tr>
                            <td>{{ $usuario->login}} </td>
                            <td>{{ $usuario->nome}} </td>
                            <td>{{ $usuario->email}} </td>
                            <td>{{ $usuario->cpf}} </td>
                            <td>{{ $usuario->perfil->valor}} </td>
                            <td >
                                <a href="/adm/usuario/form/{{ $usuario->id }}" 
                                class="w-100 d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                    <i class="fas fa-edit fa-sm"></i>
                                </a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection