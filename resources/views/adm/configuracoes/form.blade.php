@extends('template')

@section('titulo','Editar configurações')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Editar Configurações</h1>
@endsection

@section('conteudo')
<form method="POST" action="/adm/configuracoes/editar" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if (count($errors) > 0)
        <div id="alert-message" onclick="esconder('#alert-message')" class="alert alert-danger" role="alert" >
            <div style="cursor: pointer" id="fecha-message" data-toggle="tooltip" data-placement="left" title="Fechar"  class="text-danger  close h-100 h-100">
                <span id="mensagem">x</span>
            </div>
            @foreach ($errors->all() as $erro)
                <span>{{ $erro }} </span><br>               
            @endforeach          
        </div>
    @endif
    <div class="form-group row mb-0">
        <div class="form-group col-6 mb-0">
            <input type="hidden" value="{{ $configuracao->id }}" name="id">
            <div class="form-group">
            <label for="chave"> Chave{{ $configuracao->id == 0 ? '* (desejável um nome curto e sem espaços)' : '(não editável)' }}:</label>
                @if ($configuracao->id != null  && $configuracao->id > 0 )
                <input class="form-control" type="text" readonly value="{{ $configuracao->chave }}" name="chave">
                @else
                <input class="form-control" type="text" value="{{ $configuracao->chave }}" name="chave" required>
                @endif
            </div>
            
        </div>
        <div class="form-group col-6">
            <label for="valor">Valor*:</label>
            @if ($configuracao->chave == 'logo-instituicao')
            <input class="form-control" type="file" accept=".png, svg, jpeg, .jpg" aria-label="procurar" value="{{ $configuracao->valor }}" name="arquivo" required>
            @else
            <input class="form-control" type="text" value="{{ $configuracao->valor }}" name="valor" required>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="descricao">Descrição:</label>
        <input class="form-control" type="text" value="{{ $configuracao->descricao }}" name="descricao">
    </div>

    <div class="form-group row col-12 pl-0 pt-4">
        <div class="form-group col-2 ">
            <input type="submit" class="btn btn-primary w-100" value="Salvar">
        </div>
        <div class="form-group col-2">
            <a href="/adm/configuracoes" class="btn  btn-danger w-100">Cancelar</a>
        </div>
    </div>
</form>
@endsection