@extends('template')

@section('titulo','Dashboard')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Dashboard</h1>
@endsection

@section('conteudo')

    <div class="row justify-content-center">

        @component('components.smallCard')
            @slot('iconHeader','graduation-cap')
            @slot('titulo','Certificados Pendentes')
            @slot('valor', $certificadosPendentes);
            @slot('id', 'certificados');
            @slot('icon', 'fa fa-graduation-cap fa-5x')
            @slot('cardLink', '/aluno/certificado')
            @slot('cor','warning')
        @endcomponent

        @component('components.smallCard')
            @slot('iconHeader','clock')
            @slot('titulo','Total de horas')
            @slot('valor', $totalHoras);
            @slot('id', 'horas');
            @slot('cardLink', '/aluno/certificado')
            @slot('icon', 'fa fa-clock fa-5x')
            @slot('cor','success')
        @endcomponent

        @component('components.smallCard')
            @slot('iconHeader','chart-line')
            @slot('titulo','Pontuacao')
            @slot('valor', $pontuacao);
            @slot('id', 'pontuacao-exigida');
            @slot('cardLink', '/aluno/certificado')
            @slot('icon', 'fa fa-chart-line fa-5x')
            @slot('cor','info')
        @endcomponent

    </div>


    <div class="row justify-content-center">

    <div class="col-12 pr-2">
        <div class="card shadow border-left-primary mb-4">
            <div class="card-header py-3 ">
                <div class="container row justify-content-left h-100 w-100 px-0">
                    <div class=" px-2 text-primary">
                        <i class="fas fa-fw fa-chart-pie "></i>
                    </div>
                    <h6 class="m-0 font-weight-bold text-primary">Gráfico de Pontuação</h6>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4">
                    <canvas id="grafico"></canvas>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/grafico-circular.js') }}"></script>
