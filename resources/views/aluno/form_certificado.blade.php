@extends('template')

@section('titulo','Enviar Certificado')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Dados do Certificado</h1>
@endsection

@section('conteudo')

    <form method="POST" action="/aluno/certificado/enviar" enctype="multipart/form-data" id="form-certificado">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="curso">Curso*:</label>
            <input type="text" name="curso" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="instituicao">Instituição*:</label>
            <input class="form-control" type="text" name="instituicao" required> 
        </div>

        <div class="form-group">
            <label for="carga_horaria">Carga Horária*:</label>
            <input class="form-control" type="text"  name="carga_horaria" required>
        </div>

        <div class="form-group">
            <label for="carga_horaria">Data Conslusão*:</label>
            <input class="form-control" type="date" id="data_conclusao" name="data_conclusao" required onblur="validaDataConclusao()">
        </div>

        <div class="form-group">
            <label for="carga_horaria">Codigo de Verifição:</label>
            <input class="form-control" type="text"  name="cod_verificacao">
        </div>

        <div class="form-group">
            <label for="arquivo">Carregar arquivo*:</label>
            <input class="form-control" type="file" accept=".pdf" aria-label="procurar" name="caminho_arquivo" required>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Enviar Certificado">
        </div>

        <small class="text-disabled">* Campos Obrigatórios</small>
    </form>

<script>
    

    function validaDataConclusao() {
        let dataConclusao = document.getElementById('data_conclusao');
        let data = parseDate(dataConclusao.value);
        let agora = new Date();
        if ( data > agora ) {
            dataConclusao.setCustomValidity('Data de conclusão não pode ser futura.');
        } else {
            dataConclusao.setCustomValidity('');
        }
    }
    
    function parseDate(s) {
        var b = s.split(/\D/);
        return new Date(b[0], --b[1], b[2]);
    }
</script>

@endsection
