@extends('template')

@section('titulo','Meus Certificados')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Meus Certificados</h1>
@endsection

@section('conteudo')

@if(!empty($certificados))
    <hr>
    <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Curso</th>
                        <th>Instituição</th>
                        <th>Carga Horária</th>
                        <th>Aluno</th>
                        <th>Status</th>
                        <th>Avaliado Por</th>
                        <th>Data Avaliação</th>
                        <th>Motivo Recusa</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($certificados as $certificado)
                       
                        <tr>
                            <td>{{ $certificado->curso}} </td>
                            <td>{{ $certificado->instituicao}} </td>
                            <td>{{ $certificado->carga_horaria}} </td>
                            <td>{{ $certificado->aluno->nome}} </td>
                            <td>{{ $certificado->status}} </td>
                            <td>{{ $certificado->coordenador != null?$certificado->coordenador->nome:''}} </td>
                            <td>{{ $certificado->data_avaliado}} </td>
                            <td>{{ $certificado->motivo_recusa}} </td>
                            <td>{{ $certificado->observacao}} </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    @else
    Nenhum certificado encontrado
    @endif
@endsection