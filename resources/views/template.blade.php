<!DOCTYPE html>
<html lang="en">
    
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('titulo')</title>

    <!-- Custom fonts for this template -->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }} " rel="stylesheet">

</head>

<body id="page-top" class="sidebar-toggled">
    
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">
            
            <!-- Sidebar - Brand -->
            <li class="nav-item">
            <a class="sidebar-brand d-flex align-items-center justify-content-center text-white bg-white p-0 pr-2 pl-2" href="/{{ session('rotas')[0]->path }}">
                    <div class="sidebar-brand-icon w-100 h-75 d-flex py-1">
                        @php                            
                            $assetPath = asset("img");
                            //dd(session('logo-path'));
                        @endphp 
                        <img src="{{ $assetPath.'/'.session('logo-path') }}" class="w-100 h-100  " alt="">                
                    </div>  
                    <!-- div class="sidebar-brand-text mx-3">Projeto CDL</div -->
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            @foreach(session('rotas') as $rota)
            <li class="nav-item">
                <a class="nav-link" href="/{{$rota->path}}">
                    <i class="fas fa-fw {{$rota->icone}}"></i>
                    <span>{{ $rota->label}}</span></a>
            </li>
            <hr class="sidebar-divider my-0">
            @endforeach
            <li class="nav-item">
                <a class="nav-link" href="/sobre">
                    <i class="fas fa-fw fa-info"></i>
                    <span>Sobre</span></a>
            </li>
            <hr class="sidebar-divider my-0">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline mt-3">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                
                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow pl-0">
                    <div class="topbar-divider d-none d-sm-block ml-0"></div>

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    @yield('page-header')
                    
                    <!-- Topbar Search -->                    
                    <!-- <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form> -->

                    <!-- Topbar Navbar -->
                                
                    <ul class="navbar-nav ml-auto">
                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <!--li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <Dropdown - Messages >
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li -->

                        <!-- Nav Item - Alerts -->
                        <li style="cursor: pointer" class="nav-item dropdown no-arrow mx-1" >
                            <a class="nav-link dropdown-toggle" onmouseover="cursorMao('nav-link')" id="alertsDropdown"  role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell fa-fw"></i>
                                <!-- Counter - Alerts -->
                                @if (session()->has('notificacoes') and sizeof(session('notificacoes')) > 0)
                                <span class="badge badge-danger badge-counter"
                                    id="notificacaoCounter">{{sizeof(session('notificacoes'))}}</span>
                                @endif
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="alertsDropdown">
                                <h6 class="dropdown-header">
                                    Notificações
                                </h6>
                                @if (session()->has('notificacoes') and sizeof(session('notificacoes')) > 0)
                                @foreach (session('notificacoes') as $notificacao)
                                <button class="dropdown-item d-flex align-items-center"
                                    onclick="sendReadNotification({{$notificacao->id}})"
                                    id="item-notificacao-{{$notificacao->id}}">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-primary">
                                            <i class="fas fa-bullhorn text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">{{$notificacao->created_at}}</div>
                                        <span class="font-weight-bold">{!! nl2br(e($notificacao->mensagem)) !!}</span>
                                    </div>
                                </button>
                                @endforeach
                                @else
                                <button class="dropdown-item d-flex align-items-center">
                                    <div>
                                        <span>Nenhuma notificação</span>
                                    </div>
                                </button>
                                @endif
                                <!-- <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a> -->
                            </div>
                        </li>

                        <!-- Nav Item - Messages -->
                        <li style="cursor: pointer" class="nav-item dropdown no-arrow mx-1">
                            <a  class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-envelope fa-fw"></i>
                                <!-- Counter - Messages -->
                                @if(session()->has('mensagens') and sizeof(session('mensagens')) > 0)
                                    <span
                                        class="badge badge-danger badge-counter">{{session('mensagensNaoLidas')}}
                                    </span>
                                @endif
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="messagesDropdown">
                                <h6 class="dropdown-header">
                                    Minhas mensagens
                                </h6>
                                @if (session()->has('mensagens') and sizeof(session('mensagens')) > 0)
                                @foreach (session('mensagens') as $mensagem)
                                  <a class="dropdown-item d-flex align-items-center" href="#">
                                      <div class="dropdown-list-image mr-3 text-secondary">
                                          <i class="fas fa-user-circle fa-3x"></i>
                                      </div>
                                      <div class="font-weight-bold">
                                          <div class="text-truncate">
                                            {{$mensagem->mensagem}}
                                          </div>
                                          <div class="small text-gray-500">
                                            {{$mensagem->nome}}
                                          </div>
                                      </div>
                                  </a>
                                @endforeach
                                @else
                                <button class="dropdown-item d-flex align-items-center">
                                    <div>
                                        <span>Nenhuma mensagem</span>
                                    </div>
                                </button>
                                @endif
                            </div>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if (session()->has('usuarioLogado'))
                                <span
                                    class="mr-2 d-none d-lg-inline text-gray-600 small">{{ session('usuarioLogado')->nome }}</span>
                                <i class="fas fa-user"></i>
                                @endif
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <!-- <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a -->
                <!--a class="dropdown-item" href="/configuracoes">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Configurações
                </a-->
                <!-- a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div> -->
                                <button class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logoff
                                </button>
                            </div>
                        </li>

                    </ul>

                </nav>
                
                <!-- guarda o tempo de sessão -->
                <input type="hidden" value=" {{session('timeout')}}" id="tempo-sessao"/>
    
                <div id="infoTimeout" class="text-danger pr-4 text-right">Sua sessão expira em: <br><span id="time" value>Calculando Tempo...</span></div>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
          @yield('page-header')
        </div> -->


                    <div class="card shadow mb-4">
                        <div class="card-body">
                            @if (!empty(session('mensagem')))
                            <div id="alert-message" onclick="esconder('#alert-message')" class="alert alert-success row justify-content-md-between" role="alert">
                                {{ session('mensagem') }}
                                <div style="cursor: pointer" id="fecha-message" data-toggle="tooltip" data-placement="left" title="Fechar"  class="text-success close h-100 h-100">
                                        <span id="mensagem">x</span>
                                </div>
                            </div>
                            @endif
                            @if (!empty(session('erro')))
                            <div id="alert-message" onclick="esconder('#alert-message')" class="alert alert-danger" role="alert" >
                                {{ session('erro') }}
                                <div style="cursor: pointer" id="fecha-message" data-toggle="tooltip" data-placement="left" title="Fechar"  class="text-danger close h-100 h-100">
                                    <span id="mensagem">x</span>
                            </div>
                            </div>
                            @endif
                            @yield('conteudo')
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <div class="container text-center pb-1 ">
                        </div>
                        
                        <span class="text-center">Copyright &copy; Sistema de controle de atividades complementares acadêmicas 2020</span> 
                        <hr>
                        <div class="list-group w-100 px-1 text-center">
                            <span class="list-unstyled py-2 text-center">
                                Desenvolvido pela turma da disciplina de Programação Web 
                            </span>
                            <span class="list-unstyled py-2">
                                Curso: Análise e Desenvolvimento de Sistemas (Noite) 
                            </span>
                            <span class="list-unstyled py-2">
                                Instituição: Faculdade CDL Fortaleza-CE
                            </span>
                        </div>                   
                    </div>                    
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Logoff</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Deseja mesmo sair?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="/logoff">Logoff</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }} "></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
    <script src="{{ asset('js/demo/utils-js.js') }}"></script>
    <script>
    function sendReadNotification(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/aluno/notificacao/read/' + id,
            type: 'POST',
            success: function(data) {
                console.log(data);
                if (data > 0) {
                    $('#notificacaoCounter').text(data);
                    $('#item-notificacao-' + id).remove();
                } else {
                    $('#notificacaoCounter').remove();
                    $('#item-notificacao-' + id).text('Nenhuma notificação');

                }
            },
            error: function(data) {
                console.log('ERROR: ' + data);
            }
        });
    }; 
   
    </script>
</body>

</html>