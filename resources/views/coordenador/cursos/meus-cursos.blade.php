@extends('template')

@section('titulo','Meus Cursos')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Meus Cursos</h1>
@endsection

@section('conteudo')
    <div class="row justify-content-center">
        @foreach ($cursos as $c => $curso)
            @component('coordenador.smallCard')
            @slot('iconHeader','book')
            @slot('nomeCurso',$curso->nome)
            @slot('turno', $curso->pivot->turno)
            @slot('qtdeAlunos', $qtdeAlunos[$c])
            @slot('icon', 'fa fa-book fa-5x')
            @slot('cardLink', '/coordenador/cursos/nomeCurso/turno/periodo')
            @slot('cor',$cores[$c])
            @endcomponent
        @endforeach   
    </div>
@endsection
