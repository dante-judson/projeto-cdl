@extends('template')

@section('titulo','Listar alunos')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Lista de Alunos</h1>
@endsection

@section('conteudo')
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <td>Matricula </td>
                        <td>Nome</td>
                        <td>CPF</td>
                        <td>Email</td>
                        <td>Status</td>
                        <td>Curso</td>
                        <td>Turno</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($alunos as $aluno)                        
                        <tr>
                            <td>{{ $aluno->matricula}} </td>
                            <td>{{ $aluno->nome }} </td>
                            <td>{{ $aluno->cpf }} </td>
                            <td>{{ $aluno->email }} </td>
                            <td>{{ $aluno->status }} </td>
                            <td>{{ $curso->nome }} </td>
                            <td>{{ $aluno->turno }} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
@endsection