@extends('template')

@section('titulo','Relatório')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Relatórios</h1>
@endsection

@section('conteudo')
<div class="row justify-content-center">
    <div class="col">
        <form method="GET">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Curso:</label>
                        <input type="text" class="form-control" name="curso" value="{{$filter['curso'] ?? ''}}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Instituição:</label>
                        <input type="text" class="form-control" name="instituicao" value="{{$filter['instituicao'] ?? ''}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Carga Horária:</label>
                        <input type="number" min="0" class="form-control" name="carga_horaria" value="{{$filter['carga_horaria'] ?? ''}}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Motivo Recusa:</label>
                        <select class="form-control" name="motivo_recusa">
                            <option value="">Selecione uma opção</option>
                            <option value="DADOS_ERRADOS" {{$filter['motivo_recusa']=='DADOS_ERRADOS'?'selected':''}}>DADOS ERRADOS</option>
                            <option value="FORA_DO_PRAZO" {{$filter['motivo_recusa']=='FORA_DO_PRAZO'?'selected':''}}>FORA DO PRAZO</option>
                            <option value="INVALIDO_PARA_CURSO" {{$filter['motivo_recusa']=='INVALIDO_PARA_CURSO'?'selected':''}}>INVÁLIDO PARA CURSO</option>
                        </select>
                    </div>                
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Status:</label>
                        <select class="form-control" name="status">
                            <option value="">Selecione uma opção</option>
                            <option value="APROVADO" {{$filter['status']=='APROVADO'?'selected':''}} >APROVADO</option>
                            <option value="REPROVADO" {{$filter['status']=='REPROVADO'?'selected':''}}>REPROVADO</option>
                            <option value="PENDENTE" {{$filter['status']=='PENDENTE'?'selected':''}}>PENDENTE</option>
                        </select>
                    </div>
                </div>
                <div class="col">

                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <input type="submit" value="Pesquisar" class="btn btn-primary" formaction="/coordenador/relatorio">
                        <input type="submit" value="Dowload PDF" {{empty($certificados)?'disabled':''}}
                        class="btn btn-primary" formaction="/coordenador/relatorio/export">
                    </div>
                </div>
            </div>
            
        </form>
    </div>

</div>

<div class="row justify-content-center">

    @if(!empty($certificados))
    <hr>
    <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Curso</th>
                        <th>Instituição</th>
                        <th>Carga Horária</th>
                        <th>Aluno</th>
                        <th>Status</th>
                        <th>Avaliado Por</th>
                        <th>Data Avaliação</th>
                        <th>Motivo Recusa</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($certificados as $certificado)
                       
                        <tr>
                            <td>{{ $certificado->curso}} </td>
                            <td>{{ $certificado->instituicao}} </td>
                            <td>{{ $certificado->carga_horaria}} </td>
                            <td>{{ $certificado->aluno->nome}} </td>
                            <td>{{ $certificado->status}} </td>
                            <td>{{ $certificado->coordenador != null?$certificado->coordenador->nome:''}} </td>
                            <td>{{ $certificado->data_avaliado}} </td>
                            <td>{{ $certificado->motivo_recusa}} </td>
                            <td>{{ $certificado->observacao}} </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>

    @endif

</div>

@endsection

<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
