<html>

<head>
    <style>
    body {
        font-family: "Roboto", helvetica, arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
    }

    div.table-title {
        margin: auto;
        max-width: 600px;
        width: 100%;
    }

    .table-title h3 {
        text-align: center;
        font-size: 30px;
        font-weight: 400;
        font-style: normal;
        font-family: "Roboto", helvetica, arial, sans-serif;
        text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
        text-transform: uppercase;
    }

    /*** Table Styles **/

    .table-fill {
        border-collapse: collapse;
        max-width: 600px;
        width: 100%;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
    }

    th {
   
        font-size: 14px;
        font-weight: 400;
        padding: 10px;
        text-align: left;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        vertical-align: middle;
    }

    tr {
        font-size: 10px;
        font-weight: 400;
        text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
    }


    tr:nth-child(odd) td {
        background: #EBEBEB;
    }


    td {
        background: #FFFFFF;
        padding: 10px;
        text-align: left;
        vertical-align: middle;
        font-weight: 400;
        font-size: 10px;
    }

    .report-header {
        display: inline-block;
        width: 300px;
    }
    </style>
</head>

<body>

    <div class="table-title">
        <h3>Relatório de certificados</h3>
    </div>
    <hr>
    <div>
        <span class="report-header">
            Gerado por: {{$geradoPor}}
        </span>
        <span class="report-header">
            Curso: {{$filter['curso'] ?? 'TODOS'}}
        </span>
        <span class="report-header">
            Instituição: {{$filter['instituicao'] ?? 'TODAS'}}
        </span>
        <span class="report-header">
            Carga Horária: {{$filter['carga_horaria'] ?? 'TODAS'}}
        </span>
    </div>
    <div>
        <span class="report-header">
            Data de geração: {{$dataGerado}}
        </span>
        <span class="report-header">
            Motivo da Recusa: {{$filter['motivo_recusa'] ?? 'TODOS'}}
        </span>
        <span class="report-header">
            Status: {{$filter['status'] ?? 'TODOS'}}
        </span>

    </div>
    <hr>

    @if(!empty($certificados))
            <table class="table-fill">
                <thead>
                    <tr>
                        <th>Curso</th>
                        <th>Instituição</th>
                        <th>Carga Horária</th>
                        <th>Aluno</th>
                        <th>Status</th>
                        <th>Avaliado Por</th>
                        <th>Data Avaliação</th>
                        <th>Motivo Recusa</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($certificados as $certificado)
                       
                       <tr>
                           <td>{{ $certificado->curso}} </td>
                           <td>{{ $certificado->instituicao}} </td>
                           <td>{{ $certificado->carga_horaria}} </td>
                           <td>{{ $certificado->aluno->nome}} </td>
                           <td>{{ $certificado->status}} </td>
                           <td>{{ $certificado->coordenador != null?$certificado->coordenador->nome:''}} </td>
                           <td>{{ $certificado->data_avaliado}} </td>
                           <td>{{ $certificado->motivo_recusa}} </td>
                           <td>{{ $certificado->observacao}} </td>
                       </tr>

                   @endforeach

  
                </tbody>
            </table>

    @endif

    <script type="text/php">
    if (isset($pdf)) {
        $text = "Página {PAGE_NUM} de {PAGE_COUNT}";
        $size = 10;
        $font = $fontMetrics->getFont("Roboto");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width) / 2;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
    }
</script>
</body>

</html>