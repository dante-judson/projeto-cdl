
<div class="col-xl-5 col-md-10 mx-md-3 col-sm-10 mb-6 py-3 ">
    <div class="card border-left-{{ $cor }} shadow h-100 p-0 col-12 ">
        <div class="card-header pt-3 pb-2">
            <div class="container column text-{{ $cor }} align-items-center justify-content-between h-100 w-100 px-0">
                <div class="row pb-2 pl-3 align-items-center text-uppercase">
                    <i class="fas fa-fw fa-{{ $iconHeader }} text-sm-center text-md-center"></i>
                    <span  class="m-0 font-weight-bold text-sm-center text-md-left"><span class="px-2">{{ $nomeCurso }}</span>
                    -<span class="px-2"> {{ $turno }}</span>
                    <input type="hidden" value=" {{ $nomeCurso }}" name="nomeCurso">
                </div>
                <input type="hidden" value=" {{ $turno }}" name="turno">
            </div>
        </div>
        <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="row justify-content-center">
                    <div class="row justify-content-center">
                        @if(isset($icon))
                            <!--i class="{{$icon.' mb-3 '}}"></i-->
                            <i class="fas fa-fw fa-user mb-3 fa-4x ' text-{{ $cor }}"></i>
                        @endif
                    </div>                    
                </div>   
                @if(isset($qtdeAlunos))
                <div class="column text-center justify-content-center" >
                    <div class="h1 abnf mb-1 font-weight-bold text-grey-800 "> {{ $qtdeAlunos }}</div>
                    <div class="h5 abnf mb-1 font-weight-bold text-grey-800 "> Aluno(s)</div>
                    <input type="hidden" value="{{ $qtdeAlunos }}" name="qtdeAlunos">
                </div>
                @endif              
            </div>
        </div>
            <hr>
            <div class="row justify-content-center">
                <a class="btn btn-{{$cor}} text-white" href="/coordenador/cursos/{{ $nomeCurso }}/{{ $turno }}">
                    Visualizar grade de alunos
                </a>
            </div>
        </div>
    </div>
</div>