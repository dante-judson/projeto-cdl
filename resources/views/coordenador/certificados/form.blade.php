@extends('template')

@section('titulo','Dashboard')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Certificados Pendentes</h1>
@endsection

@section('conteudo')

    <div class="row">
        <div class="col">
                <div class="form-group">
                    <label>Nome do aluno:</label>
                    <input type="text" class="form-control" readonly value="{{$certificado->aluno->nome}}">
                </div>

                <div class="form-group">
                    <label>Matricula:</label>
                    <input type="text" class="form-control" readonly value="{{$certificado->aluno->matricula}}">
                </div>

                <div class="form-group">
                    <label>Instituição:</label>
                    <input type="text" class="form-control" readonly value="{{$certificado->instituicao}}">
                </div>

                <div class="form-group">
                    <label>Curso:</label>
                    <input type="text" class="form-control" readonly value="{{$certificado->curso}}">
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label>Carga Horária:</label>
                        <input type="text" class="form-control" readonly value="{{$certificado->carga_horaria}}">
                    </div>

                    <div class="form-group col">
                        <label>Data conclusão:</label>
                        <input type="text" class="form-control" readonly value="{{$certificado->data_conclusao}}">
                    </div>
                </div>

                <div class="form-group">
                    <label>Código de Verificação:</label>
                    <input type="text" class="form-control" readonly value="{{$certificado->cod_verificacao}}">
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#aprovarModal">Aprovar</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#reprovarModal">Reprovar</button>
                </div>
        
        </div>
        <div class="col">
            <embed src ="{{ action('CoordenadorController@pdfPreview', ['id'=> $certificado->id]) }}" style="position: relative; height: 100%; width: 100%;"></embed>
        </div>
    </div>

@endsection

<!-- Modal Aprovação -->
<div class="modal fade" id="aprovarModal" tabindex="-1" role="dialog" aria-labelledby="aprovarModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="aprovarModalLabel">Aprovar Certificado</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Confirmar aprovação?</div>
      <div class="modal-footer">
      <form action="/coordenador/certificado/aprova/{{ $certificado->id }}" method="post">
        {{csrf_field()}}
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary">Confirmar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal Aprovação -->

<!-- Modal Reprovação -->
<div class="modal fade" id="reprovarModal" tabindex="-1" role="dialog" aria-labelledby="reprovarModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reprovarModalLabel">Reprovar Certificado</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/coordenador/certificado/reprova/{{ $certificado->id }}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label>Motivo:</label>
                <select name="motivo_recusa" class="form-control"> 'DADOS ERRADOS', 'FORA DO PRAZO', 'INVÁLIDO PARA CURSO'
                    <option value="DADOS_ERRADOS">Dados Errados</option>
                    <option value="FORA_DO_PRAZO">Fora do prazo</option>
                    <option value="INVALIDO_PARA_CURSO">Inválido para curso</option>
                </select>
            </div>

            <div class="form-group">
                <label>Observação:</label>
                <textarea name="observacao" cols="30" rows="10" class="form-control"></textarea>
            </div>

          </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Reprovar</button>
        </div>
    </form>
    </div>
  </div>
</div>
<!-- Modal Reprovação -->


<script>
    function openCertificado(id){
        location.href = `form/${id}`;
    }
</script>
<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
