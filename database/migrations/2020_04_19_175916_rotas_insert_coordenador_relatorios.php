<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Rota;
use App\Perfil;

class RotasInsertCoordenadorRelatorios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $coordenador = Perfil::where('valor','COORDENADOR')->first()->id;

        $rota = new Rota('coordenador/relatorio/*', $coordenador, null, null, false);
        $rota->save();

        $rota = new Rota('coordenador/relatorio', $coordenador, 'Relatorios', 'fa-chart-line', true);
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
