<?php

use App\Configuracao;
use Illuminate\Database\Migrations\Migration;

class ConfiguracaoInsertSessionTimeout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = new Configuracao( 'session_timeout', '20', 'Tempo limite de sessão para o usuário');
        $config->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
