<?php

use App\Perfil;
use App\Rota;
use Illuminate\Database\Migrations\Migration;

class InsertRotasCursosAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = Perfil::where('valor', 'ADMINISTRADOR')->first()->id;

        $rota = new Rota('adm/cursos', $admin, 'Cursos', 'fa-book', true);
        $rota = new Rota('adm/cursos/adicionar', $admin, null, null, false);
        $rota = new Rota('adm/cursos/editar/*', $admin, null, null, false);
        $rota = new Rota('adm/cursos/upsert', $admin, null, null, false);
        
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
