<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create(
                'turmas', function (Blueprint $table) {
                    $table->id();            
                    $table->unsignedBigInteger('usuario_id');
                    $table->unsignedBigInteger('curso_id');
                    $table->string('turno');
                    $table->foreign('usuario_id')->references('id')
                        ->on('usuarios')->onDelete('cascade');
                    $table->foreign('curso_id')->references('id')
                        ->on('cursos')->onDelete('cascade');
                    $table->timestamps();
                }
            );
    
            DB::statement("ALTER TABLE turmas ADD CONSTRAINT chk_turno CHECK (turno='Manhã' OR turno='Tarde' OR turno='Noite');");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_do_usuarios');
    }
}
