<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('matricula')->unique()->nullable();
            $table->string('login')->unique();
            $table->string('nome');
            $table->string('senha')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('cpf',11)->unique()->nullable();
            $table->string('status');
            $table->unsignedBigInteger('perfil_id');
            $table->foreign('perfil_id')->references('id')->on('perfils');
            $table->timestamps();
        });

        // Add the constraint
        DB::statement("ALTER TABLE usuarios ADD CONSTRAINT chk_status_usuario CHECK (status='ATIVO' OR status='INATIVO' OR status='SUSPENSO');");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
