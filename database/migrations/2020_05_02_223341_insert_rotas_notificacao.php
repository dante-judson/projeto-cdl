<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Rota;
use App\Perfil;

class InsertRotasNotificacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $aluno = Perfil::where('valor','ALUNO')->first()->id;

        $rota = new Rota('aluno/notificacao/read/*', $aluno, null, null, false);
        $rota->save(); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
   
    }
}
