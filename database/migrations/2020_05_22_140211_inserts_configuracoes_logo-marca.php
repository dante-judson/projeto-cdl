<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Configuracao;

class InsertsConfiguracoesLogoMarca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = new Configuracao('logo-instituicao', 'logo-f-cdl.png',
         'imagem que representa a logomarca da instituição, que aparecerá na parte superior esquerda da tela.'
        );

        $config->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
