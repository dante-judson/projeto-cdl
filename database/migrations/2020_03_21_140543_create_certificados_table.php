<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificados', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->string('curso');
            $table->string('instituicao');
            $table->integer('carga_horaria');
            $table->date('data_conclusao');
            $table->date('data_envio');
            $table->date('data_avaliado')->nullable();
            $table->string('motivo_recusa')->nullable();
            $table->string('observacao')->nullable();
            $table->string('cod_verificacao')->unique()->nullable();
            $table->string('caminho_arquivo')->unique()->nullable();
            $table->unsignedBigInteger('aluno_id');
            $table->foreign('aluno_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('coordenador_id')->nullable();
            $table->foreign('coordenador_id')->references('id')->on('usuarios');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE certificados ADD CONSTRAINT chk_status_certificado CHECK (status in ('PENDENTE', 'APROVADO', 'REPROVADO'));");
        DB::statement("ALTER TABLE certificados ADD CONSTRAINT chk_motivo_recusa CHECK (motivo_recusa in ('DADOS ERRADOS', 'FORA DO PRAZO', 'INVÁLIDO PARA CURSO'));");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificados');
    }
}
