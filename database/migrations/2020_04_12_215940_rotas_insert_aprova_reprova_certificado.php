<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Perfil;
use App\Rota;

class RotasInsertAprovaReprovaCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $coordenador = Perfil::where('valor','COORDENADOR')->first()->id;

        $rota = new Rota('coordenador/certificado/aprova/*', $coordenador, null, null, false);
        $rota->save();

        $rota = new Rota('coordenador/certificado/reprova/*', $coordenador, null, null, false);
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
