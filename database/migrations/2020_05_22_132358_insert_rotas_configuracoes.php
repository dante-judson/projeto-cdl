<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Rota;
use App\Perfil;

class InsertRotasConfiguracoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $administrador = Perfil::where('valor','ADMINISTRADOR')->first()->id;

        $rota = new Rota('adm/configuracoes', $administrador, 'Configurações', 'fa-cogs', true);
        $rota = new Rota('adm/configuracoes/form', $administrador, 'Configuracoes', null, false);
        $rota = new Rota('adm/configuracoes/editar', $administrador, null, null, false);
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
