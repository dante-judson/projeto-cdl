<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Rota;
use App\Perfil;


class RotasInsertDonwloadPdfRota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //'coordenador/relatorio/export*',0,2
        $coordenador = Perfil::where('valor','COORDENADOR')->first()->id;

        $rota = new Rota('coordenador/relatorio/export*', $coordenador, null, null, false);
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
