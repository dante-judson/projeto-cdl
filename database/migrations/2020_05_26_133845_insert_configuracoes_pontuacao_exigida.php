<?php

use Illuminate\Database\Migrations\Migration;
use App\Configuracao;

class InsertConfiguracoesPontuacaoExigida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {       
        $config = new Configuracao('pontuacaoExigida', '20', 'Pontuação exigida para conclusão do curso');
        $config->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
