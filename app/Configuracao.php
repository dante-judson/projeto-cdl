<?php

namespace App;

class Configuracao extends AuditableModel
{
    function __construct() {

        if (func_num_args() == 3) {
            $this->chave = func_get_arg(0);
            $this->valor = func_get_arg(1);
            $this->descricao = func_get_arg(2);
        }
    }
}
