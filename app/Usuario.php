<?php

namespace App;

use App\Enums\UsuarioStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OwenIt\Auditing\Contracts\UserResolver;
use Exception;

class Usuario extends AuditableModel implements  UserResolver
{

    public function perfil() {
        return $this->belongsTo('App\Perfil');
    }

    public static function encryptPassword ($rawPass) {
        $options = [
            'salt' => Usuario::generateSalt()
        ];

        return password_hash($rawPass, PASSWORD_BCRYPT);
    }

    public static function login ($login, $senha ) {
        try {
            $usuario = Usuario::getUsuarioAtivoByLogin($login);
            if (!Usuario::checkPassword($senha, $usuario->senha)) {
                throw new Exception('Usuario ou senha inválidos');
            } else {
                return $usuario;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    public static function getUsuarioByLogin($login) {
        try {
            $usuario = Usuario::where('login', $login)->firstOrFail();
            return $usuario;
        } catch(ModelNotFoundException $e) {
            error_log($e->getMessage());
            throw new Exception('Usuario ou senha inválidos');
        }
    }

    public static function getUsuarioAtivoByLogin($login) {
        $usuario = Usuario::getUsuarioByLogin($login);
        if ($usuario->status != UsuarioStatusEnum::ATIVO) {
            throw new Exception('Usuário está ' . $usuario->status . ' no sistema, entre em contato com a faculdade em caso de dúvida.');
        }
        return $usuario;
    }

    public static function checkPassword ($rawPass, $passwordHash ) {
        return password_verify($rawPass, $passwordHash);
    }

    private static function generateSalt () {
        return rand(22,100);
    }

    public static function resolve() {
        return session()->has('usuarioLogado') ? session('usuarioLogado'): null;
    }

    public static function resolveId()
    {
        return session()->has('usuarioLogado') ? session('usuarioLogado')->id : null;
    }

    public function getAuthIdentifier() {
        return session()->has('usuarioLogado') ? session('usuarioLogado')->id : null;
    }

    public function cursosWithPivot($coordenador_id) {
        return $this->belongsToMany(Curso::class, 'turmas')
            ->where(
                ['turmas.usuario_id' => $coordenador_id]
            )
            ->withPivot(['id', 'turno'])
            ->get()
            ->all();
    }

    public function cursos() {
        return $this->belongsToMany(Curso::class, 'turmas')
            ->get()
            ->all();
    }

    public function getDefaultRoute() {
        if ($this->perfil->valor == 'COORDENADOR') {
            return '/coordenador';
        } else if ($this->perfil->valor == 'ALUNO') {
            return '/aluno';
        } else if ($this->perfil->valor == 'ADMINISTRADOR') {
            return '/adm/usuario';
        }    
    }
}