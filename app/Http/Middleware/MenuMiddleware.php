<?php

namespace App\Http\Middleware;

use Closure;

use App\Rota;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('usuarioLogado')) {
            $usuario = session('usuarioLogado');

            if (!session()->has('rotas')) {
                $rotas = Rota::where([
                    ['perfil_id',$usuario->perfil_id],
                    ['is_menu',true]
                ])->get();
                session()->put('rotas',$rotas);
            }

        }

        return $next($request);
    }
}
