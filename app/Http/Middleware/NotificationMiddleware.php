<?php

namespace App\Http\Middleware;

use Closure;

use App\Notificacao;

class NotificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('usuarioLogado')) {
            $usuario = session('usuarioLogado');
            $notificacoes = Notificacao::getNotificacoesNaoLidas($usuario->id);

            session(['notificacoes' => $notificacoes]);
        }
        return $next($request);
    }
}
