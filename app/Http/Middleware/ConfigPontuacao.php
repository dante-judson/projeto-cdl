<?php

namespace App\Http\Middleware;

use App\Configuracao;
use Closure;

class ConfigPontuacao
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $config = Configuracao::where('chave','pontuacaoExigida')->first();
        session(['pontuacaoExigida' => $config->valor]); 
        return $next($request);
    }
}
