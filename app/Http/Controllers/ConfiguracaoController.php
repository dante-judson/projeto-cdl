<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracao;
use DateTime;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Symfony\Component\Console\Input\Input;

class ConfiguracaoController extends Controller
{  
    public function listaConfiguracoes() {
        $configuracoes = Configuracao::all();
        return view('adm.configuracoes.configuracoes', compact('configuracoes'));
    }

    public function formConfiguracoes(Request $request, $id = null) {
        $configuracao = new Configuracao();
        if ($this->verificaId($id)) {
            if ($id != null && $id != 0) {
                $configuracao = Configuracao::find($id);
                return view('adm.configuracoes.form', compact('configuracao'));
            }
            else {
                return redirect('/adm/configuracoes')
                ->with('erro', "Não existe nenhuma configuração com o ID '{$id}'");
            }
        }
        else {
            return redirect('/adm/configuracoes')
            ->with('erro', "O valor informado para 'ID' deve ser um valor numérico");
        }
    }

    public function editar(Request $request) {
        $configuracao = new configuracao();   
        try {
            if ($request->input('id') != null) {
                $configuracao = configuracao::find($request->input('id'));
            }
            
            if ($request->hasFile('arquivo') && $request->file('arquivo')->isValid()) {
                $configuracao->valor = $this->validaArquivo($request);
                session(['logo-path', $configuracao->valor]);
            }
            else
            {
                $configuracao->valor = $request->input('valor');
            }             
            $configuracao->chave = $request->input('chave');
            $configuracao->descricao = $request->input('descricao');
            $configuracao->save();
            $mensagem = 'Configuração alterada com sucesso';
            return redirect('/adm/configuracoes')->with('mensagem', $mensagem);

        } catch (QueryException $e) {
            error_log($e->getMessage());
            return redirect()->back()->with('erro', 'Houve um errro ao inserir o registro!');
        }
    }

    private function validaArquivo(Request $request) {
        $this->validate($request, [
            'arquivo'   => 'required|image|mimes:jpg,jpeg,png,svg| max:768px'
            ], $this->mesagens()
        );
        $extensao = $request->arquivo->extension();
        $data = new DateTime();
        $nomeArquivo = "logo-instituicao-{$data->format('dmYHis')}.{$extensao}"; 
        $request->arquivo->move(public_path('img'), $nomeArquivo);  
        return $nomeArquivo;
    }

    private function mesagens(){
        return $mensagens = array(
        'required'  => 'O campo VALOR é obrigatório, informe um arquivo',
        'mimes'     => 'Só são aceitas imagens com os formatos: :values',
        'image'     => 'O campo só permite imagens',
        'max'       => 'O tamanho máximo do arquivo deve ser de :max'
        );
    }

    /**
     * Verifica se há alguma vetra na string
     * Se ouver alguma letra, retorna false se não retorna true
     * @param string 
     * @return bool  
     **/
    public function verificaId($string) {
        $cont = 0;
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ([\'\\\/!@#$%¨&*]{_+-=*´~;.,:?^`})';
        for ($i = 0; $i < strlen($string); $i++) {
            for ($j = 0; $j < strlen($characters); $j++) {
                if ($string[$i] == $characters[$j]) {
                    $cont++;   
                }
            }
        }
        return $cont > 0 ? false : true;
    }
}
