<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\Request;

class Turma extends AuditableModel
{
    function __construct() {

        if (func_num_args() == 3) {
            $this->usuario_id = func_get_arg(0);
            $this->curso_id = func_get_arg(1);
            $this->turno = func_get_arg(2);
        }
    }

    public function alunos() {
        return $this->hasMany('App\Usuario');
    }  

    public function coordenadores() {
        return $this->hasMany('App\Usuario');
    } 
}