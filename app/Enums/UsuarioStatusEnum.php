<?php

namespace App\Enums;

class UsuarioStatusEnum {
    public const ATIVO = 'ATIVO';
    public const INATIVO = 'INATIVO';
    public const SUSPENSO = 'SUSPENSO';
}