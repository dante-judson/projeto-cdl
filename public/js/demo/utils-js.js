
// esconde a mensagem da tela
function esconder(id){
    document.querySelector(id).classList.add("d-none"); 
}

// esconde a mensagem da tela
function esmaecerEsconder(id, cont, tempo){
    if (cont == tempo) {
        jQuery(function () {
            $(id).css('transition', '1s');
            $(id).css('opacity', '0');
        }); 
    }
    else if (cont == tempo + 1) {
        esconder(id); 
    }
}

// muda o cursor para 'mao'
function cursorMao(element){
    document.getElementByClassName(element).style.cursor = "pointer";
    
};

// muda o estilo do tooltip
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
}); 

// Configurações do relógio da sessão
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    var cont = 0;
    var intervalo = setInterval( function () {
    cont++;
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    display.textContent = minutes + ":" + seconds;
    if (--timer < 0) {
        // parar a função setInterval()
        clearInterval( intervalo );  

        timer = 0;
        document.querySelector('#infoTimeout').innerHTML = "<span id='time'></span>";
        document.querySelector('#time').innerHTML = "Seu tempo de sessão expirou! Faça Login novamente!";
    } 
    esmaecerEsconder('#alert-message', cont, 8);
    }, 1000);

};

// recarrega o tempo quando o navegador atualiza
window.onload = function () {
    var tempo = document.querySelector('#tempo-sessao').value;
    var tempoDeSessao = 60 * tempo,
    display = document.querySelector('#time');
    startTimer(tempoDeSessao, display);
};