<?php

use Illuminate\Support\Facades\Route;

use App\Usuario;
use App\Perfil;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ 'uses' => 'LoginController@index']);

Route::post('/login', [ 'uses' => 'LoginController@login' ]);

Route::get('/logoff', [ 'uses' => 'LoginController@logoff' ]);

Route::get('/aluno', [ 'uses' => 'AlunoController@dashboard' ]);

Route::get('/adm/usuario', [ 'uses' => 'UsuarioController@listaUsuarios' ]);

Route::get('/adm/usuario/form/{id?}', [ 'uses' => 'UsuarioController@formUsuario']);

Route::post('/adm/usuario/upsert', [ 'uses' => 'UsuarioController@upsert' ]);

Route::get('/adm/configuracoes', [ 'uses' => 'ConfiguracaoController@listaConfiguracoes' ]);

Route::get('/adm/configuracoes/form/{id?}', ['uses' => 'ConfiguracaoController@formConfiguracoes' ]);

Route::post('/adm/configuracoes/editar', ['uses' => 'ConfiguracaoController@editar' ]);

Route::get('/adm/curso', ['uses' => 'CursoContoller@listarTodos' ]);

Route::post('/adm/configuracoes/editar', ['uses' => 'ConfiguracaoController@editar' ]);

Route::get('/aluno/certificado/enviar', [ 'as' => 'aluno.form_certificado', 'uses' => 'AlunoController@certificadoForm' ]);

Route::post('/aluno/certificado/enviar', [ 'as' => 'aluno.form_certificado.enviar ', 'uses' => 'AlunoController@enviarCertificado' ]);

Route::get('/aluno/certificado', [ 'uses' => 'AlunoController@listarCertificados' ]);

Route::get('/coordenador', [ 'uses' => 'CoordenadorController@dashboard' ]);

Route::get('/coordenador/certificado', [ 'uses' => 'CoordenadorController@listaCertificadosPendentes' ]);

Route::get('/coordenador/certificado/form/{id}', [ 'uses' => 'CoordenadorController@formCertificados' ]);

Route::post('/coordenador/certificado/aprova/{id}', [ 'uses' => 'CoordenadorController@aprova' ]);

Route::get('/coordenador/certificado/preview/pdf/{id}', [ 'uses' => 'CoordenadorController@pdfPreview' ]);

Route::post('/coordenador/certificado/reprova/{id}', [ 'uses' => 'CoordenadorController@reprova' ]);

Route::get('/coordenador/relatorio', [ 'uses' => 'CoordenadorController@relatorio' ]);

Route::get('/coordenador/relatorio/export', [ 'uses' => 'CoordenadorController@exportRelatorio' ]);

Route::post('/aluno/notificacao/read/{id}', [ 'uses' => 'NotificacaoController@read' ]);

Route::get('/coordenador/cursos/meus-cursos', [ 'uses' => 'CursoController@listarTurmasCoordenador']);

Route::get('/coordenador/cursos/{nomeCurso?}/{turno?}', [ 'uses' => 'CursoController@listarAlunos']);

Route::get('/adm/cursos', 'CursoController@listarCursosAdmin');

Route::get('/adm/cursos/editar/{id?}', 'CursoController@formCursosAdmin');

Route::get('/adm/cursos/adicionar', 'CursoController@formCursosAdmin');

Route::post('/adm/cursos/upsert', 'CursoController@upsertCurso');

Route::get('/adm/turmas', 'TurmaController@listarTurmas');

Route::get('/adm/turmas/editar/{id?}', 'TurmaController@formTurma');

Route::get('/adm/turmas/adicionar', 'TurmaController@formTurma');

Route::post('/adm/turmas/upsert', 'TurmaController@upsertTurma');

Route::get('/sobre', function(){
    return view('sobre');
});